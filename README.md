![temporary logo](https://lukesmith.xyz/pix/gifs/worker.gif)

# template0


* [External links](#external-links)
* [Overview](#overview)
* [System requirements](#system-requirements)
* [Installation](#installation)
* [Usage](#usage)
* [Configuration](#configuration)
* [Release cycle](#release-cycle)
* [Bug reports](#bug-reports)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)


## External links


* [My Personal Website](https://zero.stegatxins.xyz) (currently not avaliable)
* [How I learn Japanese](https://massimmersionapproach.com/table-of-contents/stage-1/jp-quickstart-guide/)


## Overview

**temlpate0** is template file used for all zero projects.

## System requirements

- Linux 
- Windows or macOS might work, but is **currently** not officially supported

## Installation

template0 can be installed by the following command (see #1)

```bash
$ pip3 install template0
```

## Usage

```
$ template0 --help
```

## Configuration

```
$ template0 --configuration
```

## Release cycle

I'll try to keep up my daily routine of coding around 1 to 2 hours daily. 

Next major versions will be released before 12 December 2020 according to schedule. 

## Bug reports

Please use the issue tracker provided by GitLab to send us bug reports or feature requests.


Follow the template's instructions or the issue will likely be ignored or closed as invalid.

## Contributing

Any bug reports, feature request, suggestion and merge requests are welcomed and will be highly appreciated.


Please read [CONTRIBUTING.md](CONTRIBUTING.md).


## License

This project is licensed under the MIT License.


See [here](LICENSE).

## Contact

It is recommended to use the GitLab issue tracker for project-related question or discussion.

- **GitLab issue tracker** (report bugs here)
- **My personal email**: zero@stegatxins.xyz

